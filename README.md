# Crack Inspection Using YOLOV5 with Raspberry Pi 
> Aim: Minimal Prototype for Case Competition XD

## Project Overview
![Overview.io](Overview.png) 

## Current Progress
- [X] Create Skeleton Code
- [x] Labelled Dataset with LabelImg 
- [x] Sort Data into txt files
- [x] Train pre-trained YOLO model (Local)
- [ ] Test pre-trained model accuracy & Export Model 
> Failed Point: OpenCV Import requires .cfg, .weight files of model which YOLOV5 doesn't support 
- [ ] Train YOLO V3 Model (which offers .cfg, .weight) << Current Stuck Stage
- [ ] Import Model to Raspberry Pi 
- [ ] Deploy `Deploy Machine/YoloPi.py`

## DataSource 
Dataset Reference: [Data Mendeley](https://data.mendeley.com/datasets/jwsn7tfbrp/1)
> Tools for Data Labelling: [makesense.ai](https://blog.roboflow.com/labelimg/)
> Tools for Data Import: [RoboFlow](https://app.roboflow.com/)

## Directory Explanation
| Directory | Usage|
|--|--|
|Deploy Machine|Relevant Code for Remote Machine (e.g. Raspberry Pi)|
|Train Machine|Relevant Code for Custom Yolo Model Training|
## Prerequisites
- List: `Train Machine/requirement.txt` 


## YoloV3 Details
> Reference at: [Ultralytics](https://github.com/ultralytics/yolov3)
1. To KickStart training custom model - Google Colab
```bash
[Train at Google Colab](https://colab.research.google.com/drive/1UfJQwy_fCa7zCtqWJqshjpKb_Jw_tRVR?usp=sharing)
```