# Reference at: https://cloudxlab.com/blog/how-to-run-yolo-on-cctv-feed/ 

# import the necessary packages
import numpy as np
import argparse
import imutils
import time
import cv2
import os
from imutils.video import FPS
from imutils.video import VideoStream

#
##--Part 1. Define Model Parameters -----------------------------------------##
#
## ARG 1: Base Path to YOLO Directory
YOLO_PATH="/Users/nch/Program/Case_Competition/Crack_Inspection/Deploy_Machine/yolo_crack" 
OUTPUT_FILE="output/outfile.avi" ## Path & File where output goes to

## ARG 2: CLASSES - Class labels the YOLO model was trained on
labelsPath = os.path.sep.join([YOLO_PATH, "custom.names"]) #<<
LABELS = open(labelsPath).read().strip().split("\n") 

## ARG 3: CONFINDENCE - Minimum Probability to filter weak detection
CONFIDENCE=0.5

## ARG 4: THRESHOLD - Non-maxima suppression threshold
THRESHOLD=0.3

# ARG 5: COLOR - initialize a list of colors to represent each possible class label
np.random.seed(42)
COLORS = np.random.randint(0, 255, size=(len(LABELS), 3),dtype="uint8")

#
##-- Part 2. Load prederived paths to the YOLO weights and model configuration --##
#
weightsPath = os.path.sep.join([YOLO_PATH, "yolov5_crack_weight.pt"]) #<<
configPath = os.path.sep.join([YOLO_PATH, "yolov5s_cfg.yaml"]) #<<

# load our pretrained YOLO object detector and determine only the *output* layer names that we need from YOLO
print("[INFO] loading YOLO from disk...")
net = cv2.dnn.readNetFromDarknet(configPath, weightsPath)
ln = net.getLayerNames()
ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]


#
##-- Part 3: Initialize Raspberry Pi video stream, pointer to output video file, and frame dimensions --##
#
vs = VideoStream(src=0).start()
time.sleep(2.0)
fps = FPS().start()

# Initialiase Variables
writer = None
(W, H) = (None, None) # Frame Dimension
cnt=0


#
##-- Part 4: Loop over frames from the video file stream --##
#
while True:
	cnt+=1
	# read the next frame from the file
	(grabbed, frame) = vs.read()

	# if the frame was not grabbed, then we have reached the end
	# of the stream
	if not grabbed:
		break
	# if the frame dimensions are empty, grab them
	if W is None or H is None:
		(H, W) = frame.shape[:2]

	# construct a blob from the input frame and then perform a forward
	# pass of the YOLO object detector, giving us our bounding boxes
	# and associated probabilities
	blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416),
			swapRB=True,crop=False)

	# pass the blob through the network and obtain the detections and prediction	
	net.setInput(blob)
	start = time.time()
	layerOutputs = net.forward(ln)
	end = time.time()

	# show timing information on YOLO
	print("[INFO] YOLO took {:.6f} seconds".format(end - start))	

	# initialize our lists of detected bounding boxes, confidences,
	# and class IDs, respectively
	boxes = []
	confidences = []
	classIDs = []

	# loop over each of the layer outputs
	for output in layerOutputs:
		# loop over each of the detections
		for detection in output:
			# extract the class ID and confidence (i.e., probability)
			# of the current object detection
			# confidence = detection [0,0,output,2]
			scores = detection[5:]
			classID = np.argmax(scores)
			confidence = scores[classID]

			# filter out weak predictions by ensuring the detected
			# probability is greater than the minimum probability
			if confidence > CONFIDENCE:
				# scale the bounding box coordinates back relative to
				# the size of the image, keeping in mind that YOLO
				# actually returns the center (x, y)-coordinates of
				# the bounding box followed by the boxes' width and
				# height
				box = detection[0:4] * np.array([W, H, W, H])
				(centerX, centerY, width, height) = box.astype("int")

				# use the center (x, y)-coordinates to derive the top
				# and and left corner of the bounding box
				x = int(centerX - (width / 2))
				y = int(centerY - (height / 2))

				# update our list of bounding box coordinates,
				# confidences, and class IDs
				boxes.append([x, y, int(width), int(height)])
				confidences.append(float(confidence))
				classIDs.append(classID)

	# apply non-maxima suppression to suppress weak, overlapping
	# bounding boxes
	idxs = cv2.dnn.NMSBoxes(boxes, confidences, CONFIDENCE,
		THRESHOLD)

	# ensure at least one detection exists
	if len(idxs) > 0:
		# loop over the indexes we are keeping
		for i in idxs.flatten():
			# extract the bounding box coordinates
			(x, y) = (boxes[i][0], boxes[i][1])
			(w, h) = (boxes[i][2], boxes[i][3])

			# draw a bounding box rectangle and label on the frame
			color = [int(c) for c in COLORS[classIDs[i]]]
			cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
			text = "{}: {:.4f}".format(LABELS[classIDs[i]],
				confidences[i])
			cv2.putText(frame, text, (x, y - 5),
				cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)


#
##-- Part 5: Output Processed Images as Video Stream --##
#
	# check if the video writer is None
	if writer is None:
		# initialize our video writer
		fourcc = cv2.VideoWriter_fourcc(*"MJPG")
		writer = cv2.VideoWriter(OUTPUT_FILE, fourcc, 30,
			(frame.shape[1], frame.shape[0]), True)

	# write the output frame to disk
	writer.write(frame)
	# show the output frame
	cv2.imshow("Frame", cv2.resize(frame, (800, 600)))
	key = cv2.waitKey(1) & 0xFF
	#print ("key", key)
	# if the `q` key was pressed, break from the loop
	if key == ord("q"):
		break

	# update the FPS counter
	fps.update()


##-- Part 6: End all Processes --##
# stop the timer and display FPS information
fps.stop()

print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# do a bit of cleanup
cv2.destroyAllWindows()
# release the file pointers
print("[INFO] cleaning up...")
writer.release()
vs.release()