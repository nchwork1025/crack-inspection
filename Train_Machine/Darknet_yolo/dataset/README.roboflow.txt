
Cracker - v2 2020-12-01 12:10pm
==============================

This dataset was exported via roboflow.ai on December 1, 2020 at 4:14 AM GMT

It includes 1067 images.
Crack are annotated in YOLO v5 PyTorch format.

The following pre-processing was applied to each image:
* Resize to 640x640 (Fit (reflect edges))

The following augmentation was applied to create 3 versions of each source image:
* Random shear of between -15° to +15° horizontally and -15° to +15° vertically
* Salt and pepper noise was applied to 15 percent of pixels


