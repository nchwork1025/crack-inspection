
Cracker - v1 2020-11-25 1:57am
==============================

This dataset was exported via roboflow.ai on November 24, 2020 at 5:59 PM GMT

It includes 1067 images.
Crack are annotated in YOLO v5 PyTorch format.

The following pre-processing was applied to each image:
* Resize to 500x500 (Fit (reflect edges))

The following augmentation was applied to create 3 versions of each source image:
* Random shear of between -15° to +15° horizontally and -15° to +15° vertically
* Salt and pepper noise was applied to 15 percent of pixels


