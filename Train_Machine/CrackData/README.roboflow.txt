
Crack - v1 2021-01-22 3:06pm
==============================

This dataset was exported via roboflow.ai on January 22, 2021 at 7:08 AM GMT

It includes 1157 images.
Crack are annotated in YOLO v3 Darknet format.

The following pre-processing was applied to each image:
* Auto-orientation of pixel data (with EXIF-orientation stripping)
* Resize to 640x640 (Fit (reflect edges))

The following augmentation was applied to create 3 versions of each source image:
* Random rotation of between -15 and +15 degrees
* Salt and pepper noise was applied to 5 percent of pixels


